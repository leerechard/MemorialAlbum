# MemorialAlbum

#### 介绍
电子同学录
问卷星excel转word同学录
当前功能：
1. excel中的同学数据转word单个同学录数据
2. 插入附件照片
3. 使用excel填写，生成回复单个人的word同学录

当前修改需要基于自己设置的同学录问题来修改python源码；
未来可能会加入图形界面，以及针对各自的问题，自动化解决

#### 依赖工具

1.  python 3
2.  word_spell库
3.  docx库
4.  numpy, pandas



